import Vue from 'vue'
import Vuex from 'vuex'

import personStore from 'pkg_lets_person/src/store'

import template from '@/modules/template/store'
import group from '@/modules/group/store'

import getters from './getters'
// import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

// const vuexLocal = new VuexPersistence({
//   storage: window.localStorage
// })

const store = new Vuex.Store({
  modules: {
    template,
    group,
    ...personStore,
  },
  getters
  // plugins: [vuexLocal.plugin]
})

export default store
