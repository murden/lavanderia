import Vue from 'vue'
import JsonExcel from 'vue-json-excel'
import 'normalize.css/normalize.css'
import '@/styles/clean-theme/index.scss'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/pt-br'
import '@/styles/index.scss'
import App from './App'
import store from './store'
import router from './router'
import '@/icons'
import '@/permission' // Controle de Permissão
import '../mock'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTachometerAlt } from '@fortawesome/free-solid-svg-icons'
import firebasesdk from 'firebase'
import storePlugin from 'store-plugin'

library.add(faTachometerAlt)

Vue.component('downloadExcel', JsonExcel)
Vue.component('font-awesome-icon', FontAwesomeIcon)

/*Configurar o Firebase*/
const firebase = firebasesdk.initializeApp({
  apiKey: "AIzaSyBi-ZsKZPT4hpwTBHbTXQELsVb4bX0-sVs",
  authDomain: "fourseason-78b51.firebaseapp.com",
  databaseURL: "https://fourseason-78b51.firebaseio.com",
  projectId: "fourseason-78b51",
  storageBucket: "fourseason-78b51.appspot.com",
  messagingSenderId: "470881066872",
  appId: "1:470881066872:web:702704b8ca8be92e"
})

/*const messaging = firebase.messaging();*/


Vue.use(ElementUI, { locale })
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})


Vue.use(storePlugin, {
  store,
  firebase
})
