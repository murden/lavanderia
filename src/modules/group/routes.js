import { crudRoutes } from '@/modules/_common/routes/crudRoutes'

export default crudRoutes('Categorias', 'group', 'categoria')
