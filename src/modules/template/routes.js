import { crudRoutes } from '@/modules/_common/routes/crudRoutes'

export default crudRoutes('Templates', 'template', 'form')
