"use strict";
const merge = require("webpack-merge");
const prodEnv = require("./prod.env");

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // BASE_API: '"http://localhost:8000/api/v1"'
  // BASE_API: '"http://ec2-18-228-189-213.sa-east-1.compute.amazonaws.com:8000/api/v1"'
  BASE_API: '"http://ec2-18-231-52-173.sa-east-1.compute.amazonaws.com:9000/api/v1"'
});
